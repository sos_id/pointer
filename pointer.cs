// Pointer|Pointers|10020
namespace VRTK
{
    using UnityEngine;
	using UnityEngine.UI;
	using System.Collections;
	using System.Collections.Generic;
#if UNITY_5_5_OR_NEWER
    using UnityEngine.AI;


#endif
    /// <summary>
    /// The VRTK Pointer class forms the basis of being able to emit a pointer from a game object (e.g. controller).
    /// </summary>
    /// <remarks>
    /// The concept of the pointer is it can be activated and deactivated and used to select elements utilising different button combinations if required.
    ///
    /// The Pointer requires a Pointer Renderer which is the visualisation of the pointer in the scene.
    ///
    /// A Pointer can also be used to extend the interactions of an interacting object such as a controller. This enables pointers to touch (and highlight), grab and use interactable objects.
    ///
    /// The Pointer script does not need to go on a controller game object, but if it's placed on another object then a controller must be provided to determine what activates the pointer.
    ///
    /// It extends the `VRTK_DestinationMarker` to allow for destination events to be emitted when the pointer cursor collides with objects.
    /// </remarks>
    public class VRTK_Pointer : VRTK_DestinationMarker
    {
		// yooooo
		bool flag = false;
		public GameObject te,te_bg; //長寬高文字
		public GameObject gtool; //判斷刪除時所選的家具
		public GameObject gfurniturel=null;
		public GameObject menu_turn,menu; //開啟選單視窗
		public GameObject o_menu;
		public GameObject o_menu_turn;
		public GameObject o_menu_img,o_t1,o_t2;
		public GameObject move_text;
		public GameObject tool_rotation; //vive camera(eye)
		public bool move_type=false;
		public stop_grab st_gr;
		string[] ui_img = { "pic/tablecoffee", "pic/single_sofa_1", "pic/bookcase", "pic/lamp", "pic/bed_double" };
		string[] ui_t2_1 = { "pic/tf1", "pic/tf2", "pic/tf3", "pic/tf4", "pic/tf5" };
		string[] ui_f1 = { "pic/tablecoffee","pic/preview","pic/preview" };
		string[] ui_f2 = { "pic/single_sofa"};
		string[] ui_f3 = { "pic/basket","pic/bookcase","pic/box","pic/storagebox" };
		string[] ui_f4 = { "pic/floor_lamp" };
		string[] ui_f5 = { "pic/bed_double","pic/preview" };
		string[] f1={"object/table coffee","object/coffee table","object/TV_cabinet"};
		string[] f2={"object/Single sofa_p"};
		string[] f3={"object/basket","object/bookcase","object/box","object/Storage Box"};
		string[] f4={"object/floor lamp_f"};
		string[] f5={"object/bed_double_blue_b","object/bed_double_red_b"};
		string[] home = { "skybox/skybox_ch2", "skybox/sky5X2" };//判斷去誰家
		int i=0;
		int j=0;
		int chome =0;
		bool mode=false;
		bool flag_menu=false;
		bool ex=false;
		bool bhome = false;
		bool fhome = false; //判斷重複點選
		int cmove = 0; //判斷拿過家具沒

		[Header("Pointer Activation Settings")]

        [Tooltip("The specific renderer to use when the pointer is activated. The renderer also determines how the pointer reaches it's destination (e.g. straight line, bezier curve).")]
        public VRTK_BasePointerRenderer pointerRenderer;
        [Tooltip("The button used to activate/deactivate the pointer.")]
        public VRTK_ControllerEvents.ButtonAlias activationButton = VRTK_ControllerEvents.ButtonAlias.Touchpad_Press;
        [Tooltip("If this is checked then the Activation Button needs to be continuously held down to keep the pointer active. If this is unchecked then the Activation Button works as a toggle, the first press/release enables the pointer and the second press/release disables the pointer.")]
        public bool holdButtonToActivate = true;
        [Tooltip("The time in seconds to delay the pointer being able to be active again.")]
        public float activationDelay = 0f;

        [Header("Pointer Selection Settings")]

        [Tooltip("The button used to execute the select action at the pointer's target position.")]
        public VRTK_ControllerEvents.ButtonAlias selectionButton = VRTK_ControllerEvents.ButtonAlias.Touchpad_Press;
        [Tooltip("If this is checked then the pointer selection action is executed when the Selection Button is pressed down. If this is unchecked then the selection action is executed when the Selection Button is released.")]
        public bool selectOnPress = false;

        [Header("Pointer Interaction Settings")]

        [Tooltip("If this is checked then the pointer will be an extension of the controller and able to interact with Interactable Objects.")]
        public bool interactWithObjects = false;
        [Tooltip("If `Interact With Objects` is checked and this is checked then when an object is grabbed with the pointer touching it, the object will attach to the pointer tip and not snap to the controller.")]
        public bool grabToPointerTip = false;

        [Header("Pointer Customisation Settings")]

        [Tooltip("The controller that will be used to toggle the pointer. If the script is being applied onto a controller then this parameter can be left blank as it will be auto populated by the controller the script is on at runtime.")]
        public VRTK_ControllerEvents controller;
        [Tooltip("A custom transform to use as the origin of the pointer. If no pointer origin transform is provided then the transform the script is attached to is used.")]
        public Transform customOrigin;
		public bool test = false;

        protected VRTK_ControllerEvents.ButtonAlias subscribedActivationButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
        protected VRTK_ControllerEvents.ButtonAlias subscribedSelectionButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
        protected bool currentSelectOnPress;
        protected float activateDelayTimer;
        protected int currentActivationState;
        protected bool willDeactivate;
        protected bool wasActivated;
        protected uint controllerIndex;
        protected VRTK_InteractableObject pointerInteractableObject = null;

        /// <summary>
        /// The PointerEnter method emits a DestinationMarkerEnter event when the pointer enters a valid object.
        /// </summary>
        /// <param name="givenHit">The valid collision.</param>
        public virtual void PointerEnter(RaycastHit givenHit)
        {
            if (enabled && givenHit.transform && controllerIndex < uint.MaxValue)
            {
                OnDestinationMarkerEnter(SetDestinationMarkerEvent(givenHit.distance, givenHit.transform, givenHit, givenHit.point, controllerIndex));
				//Debug.Log ("111" + givenHit.transform.tag);
				StartUseAction(givenHit.transform);
            }
        }

        /// <summary>
        /// The PointerExit method emits a DestinationMarkerExit event when the pointer leaves a previously entered object.
        /// </summary>
        /// <param name="givenHit">The previous valid collision.</param>
        public virtual void PointerExit(RaycastHit givenHit)
        {
            if (givenHit.transform && controllerIndex < uint.MaxValue)
            {
                OnDestinationMarkerExit(SetDestinationMarkerEvent(givenHit.distance, givenHit.transform, givenHit, givenHit.point, controllerIndex));
                StopUseAction();
            }
        }

        /// <summary>
        /// The CanActivate method is used to determine if the pointer has passed the activation time limit.
        /// </summary>
        /// <returns>Returns true if the pointer can be activated.</returns>
        public virtual bool CanActivate()
        {
            return (Time.time >= activateDelayTimer);
        }

        /// <summary>
        /// The IsPointerActive method is used to determine if the pointer's current state is active or not.
        /// </summary>
        /// <returns>Returns true if the pointer is currently active.</returns>
        public virtual bool IsPointerActive()
        {
            return (currentActivationState != 0);
        }

        /// <summary>
        /// The ResetActivationTimer method is used to reset the pointer activation timer to the next valid activation time.
        /// </summary>
        /// <param name="forceZero">If this is true then the next activation time will be 0.</param>
        public virtual void ResetActivationTimer(bool forceZero = false)
        {
            activateDelayTimer = (forceZero ? 0f : Time.time + activationDelay);
        }

        /// <summary>
        /// The Toggle method is used to enable or disable the pointer.
        /// </summary>
        /// <param name="state">If true the pointer will be enabled if possible, if false the pointer will be disabled if possible.</param>
        public virtual void Toggle(bool state)
        {
            if (!CanActivate() || NoPointerRenderer() || CanActivateOnToggleButton(state))
            {
                return;
            }

            ManageActivationState(willDeactivate ? true : state);
            pointerRenderer.Toggle(IsPointerActive(), state);
            willDeactivate = false;
            if (!state)
            {
                StopUseAction();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            VRTK_PlayerObject.SetPlayerObject(gameObject, VRTK_PlayerObject.ObjectTypes.Pointer);
            customOrigin = (customOrigin == null ? VRTK_SDK_Bridge.GenerateControllerPointerOrigin(gameObject) : customOrigin);
            SetupController();
            SetupRenderer();
            activateDelayTimer = 0f;
            currentActivationState = 0;
            wasActivated = false;
            willDeactivate = false;
            if (NoPointerRenderer())
            {
                Debug.LogWarning("The VRTK_Pointer script requires a VRTK_BasePointerRenderer specified as the `Pointer Renderer` parameter.");
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            UnsubscribeActivationButton();
            UnsubscribeSelectionButton();
        }

        protected virtual void Start()
        {
            FindController();
        }

        protected virtual void Update()
        {
            CheckButtonSubscriptions();
            if (EnabledPointerRenderer())
            {
                pointerRenderer.InitalizePointer(this, invalidListPolicy, navMeshCheckDistance, headsetPositionCompensation);
                pointerRenderer.UpdateRenderer();
                if (!IsPointerActive())
                {
                    bool currentPointerVisibility = pointerRenderer.IsVisible();
                    pointerRenderer.ToggleInteraction(currentPointerVisibility);
                }
            }
			if (gfurniturel != null) {
				if (gfurniturel.GetComponent<stop_grab>().type==true) {
					gfurniturel.GetComponent<Rigidbody> ().isKinematic = false;
					move_text.SetActive (false);
					move_type = false;
					gfurniturel.GetComponent<stop_grab> ().setfa();
				}
			}

        }

        protected virtual bool EnabledPointerRenderer()
        {
            return (pointerRenderer && pointerRenderer.enabled);
        }

        protected virtual bool NoPointerRenderer()
        {
            return (!pointerRenderer || !pointerRenderer.enabled);
        }

        protected virtual bool CanActivateOnToggleButton(bool state)
        {
            bool result = (state && !holdButtonToActivate && IsPointerActive());
            if (result)
            {
                willDeactivate = true;
            }
            return result;
        }

        protected virtual void FindController()
        {
            if (controller == null)
            {
                controller = GetComponentInParent<VRTK_ControllerEvents>();
                SetupController();
            }

            if (controller == null)
            {
                Debug.LogError("VRTK_Pointer requires a Controller that has the VRTK_ControllerEvents script attached to it.");
            }
        }

        protected virtual void SetupController()
        {
            if (controller)
            {
                CheckButtonMappingConflict();
                SubscribeSelectionButton();
                SubscribeActivationButton();
            }
        }

        protected virtual void SetupRenderer()
        {
            if (EnabledPointerRenderer())
            {
                pointerRenderer.InitalizePointer(this, invalidListPolicy, navMeshCheckDistance, headsetPositionCompensation);
            }
        }

        protected virtual bool ButtonMappingIsUndefined(VRTK_ControllerEvents.ButtonAlias givenButton, VRTK_ControllerEvents.ButtonAlias givenSubscribedButton)
        {
            return (givenSubscribedButton != VRTK_ControllerEvents.ButtonAlias.Undefined && givenButton == VRTK_ControllerEvents.ButtonAlias.Undefined);
        }

        protected virtual void CheckButtonMappingConflict()
        {
            if (activationButton == selectionButton)
            {
                if (selectOnPress && holdButtonToActivate)
                {
                    Debug.LogWarning("Hold Button To Activate and Select On Press cannot both be checked when using the same button for Activation and Selection. Fixing by setting Select On Press to false.");
                }

                if (!selectOnPress && !holdButtonToActivate)
                {
                    Debug.LogWarning("Hold Button To Activate and Select On Press cannot both be unchecked when using the same button for Activation and Selection. Fixing by setting Select On Press to true.");
                }
                selectOnPress = !holdButtonToActivate;
            }
        }

        protected virtual void CheckButtonSubscriptions()
        {
            CheckButtonMappingConflict();

            if (ButtonMappingIsUndefined(selectionButton, subscribedSelectionButton) || selectOnPress != currentSelectOnPress)
            {
                UnsubscribeSelectionButton();
            }

            if (selectionButton != subscribedSelectionButton)
            {
                SubscribeSelectionButton();
                UnsubscribeActivationButton();
            }

            if (ButtonMappingIsUndefined(activationButton, subscribedActivationButton))
            {
                UnsubscribeActivationButton();
            }

            if (activationButton != subscribedActivationButton)
            {
                SubscribeActivationButton();
            }
        }

        protected virtual void SubscribeActivationButton()
        {
            if (subscribedActivationButton != VRTK_ControllerEvents.ButtonAlias.Undefined)
            {
                UnsubscribeActivationButton();
            }

            if (controller)
            {
                controller.SubscribeToButtonAliasEvent(activationButton, true, ActivationButtonPressed);
                controller.SubscribeToButtonAliasEvent(activationButton, false, ActivationButtonReleased);
                subscribedActivationButton = activationButton;
            }
        }

        protected virtual void UnsubscribeActivationButton()
        {
            if (controller && subscribedActivationButton != VRTK_ControllerEvents.ButtonAlias.Undefined)
            {
                controller.UnsubscribeToButtonAliasEvent(subscribedActivationButton, true, ActivationButtonPressed);
                controller.UnsubscribeToButtonAliasEvent(subscribedActivationButton, false, ActivationButtonReleased);
                subscribedActivationButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
            }
        }

        protected virtual void ActivationButtonPressed(object sender, ControllerInteractionEventArgs e)
        {
            if (EnabledPointerRenderer())
            {
                controllerIndex = e.controllerIndex;
                Toggle(true);
            }
        }

        protected virtual void ActivationButtonReleased(object sender, ControllerInteractionEventArgs e)
        {
            if (EnabledPointerRenderer())
            {
                controllerIndex = e.controllerIndex;
                if (IsPointerActive())
                {
                    Toggle(false);
                }
            }
        }

        protected virtual void SubscribeSelectionButton()
        {
            if (subscribedSelectionButton != VRTK_ControllerEvents.ButtonAlias.Undefined)
            {
                UnsubscribeSelectionButton();
            }

            if (controller)
            {
                controller.SubscribeToButtonAliasEvent(selectionButton, selectOnPress, SelectionButtonAction);
                subscribedSelectionButton = selectionButton;
                currentSelectOnPress = selectOnPress;
            }
        }

        protected virtual void UnsubscribeSelectionButton()
        {
            if (controller && subscribedSelectionButton != VRTK_ControllerEvents.ButtonAlias.Undefined)
            {
                controller.UnsubscribeToButtonAliasEvent(subscribedSelectionButton, currentSelectOnPress, SelectionButtonAction);
                subscribedSelectionButton = VRTK_ControllerEvents.ButtonAlias.Undefined;
            }
        }

        protected virtual void SelectionButtonAction(object sender, ControllerInteractionEventArgs e)
        {
            if (EnabledPointerRenderer() && (IsPointerActive() || wasActivated))
            {
                wasActivated = false;
                controllerIndex = e.controllerIndex;
                RaycastHit destinationHit = pointerRenderer.GetDestinationHit();
                AttemptUseOnSet(destinationHit.transform);
                if (destinationHit.transform && IsPointerActive() && pointerRenderer.ValidPlayArea() && !PointerActivatesUseAction(pointerInteractableObject))
                {
                    OnDestinationMarkerSet(SetDestinationMarkerEvent(destinationHit.distance, destinationHit.transform, destinationHit, destinationHit.point, controllerIndex));
                }
            }
        }

        protected virtual bool CanResetActivationState(bool givenState)
        {
            return ((!givenState && holdButtonToActivate) || (givenState && !holdButtonToActivate && currentActivationState >= 2));
        }

        protected virtual void ManageActivationState(bool state)
        {
            if (state)
            {
                currentActivationState++;
            }

            wasActivated = (currentActivationState == 2);

            if (CanResetActivationState(state))
            {
                currentActivationState = 0;
            }
        }

        protected virtual bool PointerActivatesUseAction(VRTK_InteractableObject givenInteractableObject)
        {
            return (givenInteractableObject && givenInteractableObject.pointerActivatesUseAction && givenInteractableObject.IsValidInteractableController(controller.gameObject, givenInteractableObject.allowedUseControllers));
        }

        protected virtual void StartUseAction(Transform target)
        {
            pointerInteractableObject = target.GetComponent<VRTK_InteractableObject>();
            bool cannotUseBecauseNotGrabbed = (pointerInteractableObject && pointerInteractableObject.useOnlyIfGrabbed && !pointerInteractableObject.IsGrabbed());

			//長寬高的顯示
			showdata(target.name,target);

			//建立canvas
			if (flag == false) {
				createcanvas (target);
			}

            if (PointerActivatesUseAction(pointerInteractableObject) && pointerInteractableObject.holdButtonToUse && !cannotUseBecauseNotGrabbed && pointerInteractableObject.usingState == 0)
            {
                pointerInteractableObject.StartUsing(controller.gameObject);
                pointerInteractableObject.usingState++;
            }
        }

        protected virtual void StopUseAction()
        {
            if (PointerActivatesUseAction(pointerInteractableObject) && pointerInteractableObject.holdButtonToUse && pointerInteractableObject.IsUsing())
            {
                pointerInteractableObject.StopUsing(controller.gameObject);
                pointerInteractableObject.usingState = 0;
            }
			flag = false;
			ex=true;
			fhome = true;
			Debug.Log ("eee");
        }

        protected virtual void AttemptUseOnSet(Transform target)
        {
            if (pointerInteractableObject && target)
            {
                if (PointerActivatesUseAction(pointerInteractableObject))
                {
                    if (pointerInteractableObject.IsUsing())
                    {
                        pointerInteractableObject.StopUsing(controller.gameObject);
                        pointerInteractableObject.usingState = 0;
                    }
                    else if (!pointerInteractableObject.holdButtonToUse)
                    {
                        pointerInteractableObject.StartUsing(controller.gameObject);
                        pointerInteractableObject.usingState++;
                    }
                }
            }
        }
			

		protected virtual void createcanvas(Transform target){
			Vector3 v3;
			Object sobj;
			GameObject gobj;
			Material mat;
			switch (target.tag) {
			case "f":
			case "f3":
				Destroy (gtool);
				if (cmove != 0) {
					//gfurniturel.GetComponent<Rigidbody> ().isKinematic = false;
					gfurniturel.GetComponent<Collider> ().isTrigger = false;
					move_type = false;
					move_text.SetActive (false);
				}
				v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
				sobj = Resources.Load ("tool_2");
				gobj = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				//rotation tool 1022
				//Debug.Log(tool_rotation.transform.rotation.y*100);
				gobj.transform.Rotate (0, tool_rotation.transform.rotation.y*100, 0);
				gtool = gobj as GameObject;
				gfurniturel = target.gameObject;
				cmove = 1;
				break;
			case "f1":
				Destroy (gtool);
				if (cmove != 0) {
					//gfurniturel.GetComponent<Rigidbody> ().isKinematic = false;
					gfurniturel.GetComponent<Collider> ().isTrigger = false;
					move_type = false;
					move_text.SetActive (false);
				}
				v3 = new Vector3 (target.transform.position.x, 3.0f, target.transform.position.z);
				sobj = Resources.Load ("tool_2");
				gobj = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				//Debug.Log(tool_rotation.transform.rotation.y*100);
				gobj.transform.Rotate (0, tool_rotation.transform.rotation.y*100, 0);
				gtool = gobj as GameObject;
				gfurniturel = target.gameObject;
				cmove = 1;
				break;
			case "f2":
				Destroy (gtool);
				if (cmove != 0) {
					//gfurniturel.GetComponent<Rigidbody> ().isKinematic = false;
					gfurniturel.GetComponent<Collider> ().isTrigger = false;
					move_type = false;
					move_text.SetActive (false);
				}
				v3 = new Vector3 (target.transform.position.x, 2f, target.transform.position.z);
				sobj = Resources.Load ("tool_2");
				gobj = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				//Debug.Log(tool_rotation.transform.rotation.y*100);
				gobj.transform.Rotate (0, tool_rotation.transform.rotation.y*100, 0);
				gtool = gobj as GameObject;
				gfurniturel = target.gameObject;
				cmove = 1;
				break;
			case "ui":
				v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
				sobj = Resources.Load ("object/aaa");
				gobj = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				break;
			case "delete":
				Destroy (gfurniturel);
				Destroy (target.root.gameObject);
				cmove = 0;
				break;
			case "changecolor":
				GameObject gmat,g_gmat,g_gmat1;
				if (gfurniturel.layer == 9) {
					//f1
					switch (gfurniturel.name) {
					case "table coffee":
					case "table coffee(Clone)":
							gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
							mat = Resources.Load ("meteriales/table_cofe_w", typeof(Material)) as Material;
							gmat.GetComponent<Renderer> ().material = mat;
							gfurniturel.layer = 10;
							break;
					case "coffee table":
					case "coffee table(Clone)":
							gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
							mat = Resources.Load ("meteriales/living room-coffee table_or", typeof(Material)) as Material;
							gmat.GetComponent<Renderer> ().material = mat;
							gfurniturel.layer = 10;
							break;
					case "bookcase":
					case "bookcase(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/bookcase_wtwood", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "box":
					case "box(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/box_wood", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "Storage Box":
					case "Storage Box(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Storage Box_y", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "basket":
					case "basket(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Setthebasket_bk", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "trashcan":
					case "trashcan(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/trashcan_g", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "TV_cabinet":
					case "TV_cabinet(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/blinn1", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "bed_double_red_b":
					case "bed_double_red_b(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/bed_double_red_rr", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "bed_double_blue_b":
					case "bed_double_blue_b(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						g_gmat=gmat.transform.GetChild (0).gameObject;
						g_gmat1=gmat.transform.GetChild (1).gameObject;
						mat = Resources.Load ("meteriales/bed_double_blue_blue", typeof(Material)) as Material;
						g_gmat.GetComponent<Renderer> ().material = mat;
						g_gmat1.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "Single sofa_p":
					case "Single sofa_p(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Single sofa_pr", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					case "floor lamp_f":
					case "floor lamp_f(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/floorlamp_w", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 10;
						break;
					}
				} else {
					//f2
					switch (gfurniturel.name) {
					case "Single sofa_p":
					case "Single sofa_p(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Single sofa_p", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "floor lamp_f":
					case "floor lamp_f(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/floorlamp_f", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "bed_double_blue_b":
					case "bed_double_blue_b(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						g_gmat=gmat.transform.GetChild (0).gameObject;
						g_gmat1=gmat.transform.GetChild (1).gameObject;
						mat = Resources.Load ("meteriales/bed_double_blue_b", typeof(Material)) as Material;
						g_gmat.GetComponent<Renderer> ().material = mat;
						g_gmat1.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "bed_double_red_b":
					case "bed_double_red_b(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/bed_double_red_b", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "table coffee":
					case "table coffee(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/lambert2", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "coffee table":
					case "coffee table(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/blinn1", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "bookcase":
					case "bookcase(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/bookcase_bkwood", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "box":
					case "box(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/box_or", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "Storage Box":
					case "Storage Box(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Storage Box_g", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "basket":
					case "basket(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/Setthebasket_or", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "trashcan":
					case "trashcan(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/trashcan_p", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					case "TV_cabinet":
					case "TV_cabinet(Clone)":
						gmat = gfurniturel.transform.GetChild (0).gameObject; //Material
						mat = Resources.Load ("meteriales/lambert2", typeof(Material)) as Material;
						gmat.GetComponent<Renderer> ().material = mat;
						gfurniturel.layer = 9;
						break;
					}
				}
				Destroy (target.root.gameObject);
				break;
			case "cancel":
				Destroy (target.root.gameObject);
				break;
			case "move":
				Destroy (target.root.gameObject);
				gfurniturel.GetComponent<Rigidbody> ().isKinematic = true;
				gfurniturel.GetComponent<Collider> ().isTrigger = true;
				move_type = true;
				move_text.SetActive (true);
				break;
			default:
				break;
			}
		}

		protected virtual void showdata(string str,Transform target){
			CancelInvoke ();
			GameObject Gcamera = Camera.main.gameObject;
			Skybox skybox = Gcamera.GetComponent<Skybox> ();
			Vector3 v3;
			Object sobj;
			string sss = "";
			switch (str){
			  //cream house
			  case "vWall1":
				sss = "";
				break;
		   	  case "vWall2":
				sss = "390cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall3":
				sss = "463cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall4":
				sss = "150cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall5":
				sss = "184cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall6":
				sss = "390cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall7":
				sss = "398cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall8":
				sss = "68cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall9":
				sss = "390cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall10":
				sss = "463cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "vWall11":
				sss = "614cm x 265cm";
				te_bg.SetActive (true);
				break;
		  	  case "vWall12":
				sss = "320cm x 265cm";
				te_bg.SetActive (true);
	  			break;
			  case "hWall1":
				sss = "284cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall2":
				sss = "390cm x 265xm";
				te_bg.SetActive (true);
				break;
			  case "hWall3":
				sss = "198cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall4":
				sss = "198cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall5":
				sss = "68cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall6":
				sss = "548cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall7":
				sss = "300cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall8":
				sss = "140cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall9":
				sss = "180cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall10":
				sss = "180cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall11":
				sss = "300cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall12":
				sss = "264cm x 265cm";
				te_bg.SetActive (true);
				break;
			  case "hWall13":
				sss = "120cm x 265cm";
				te_bg.SetActive (true);
				break;
			//fun house start
			case "fvwall1":
				sss = "510.3cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fvwall2":
				sss = "212.6cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fvwall3":
				sss = "176cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fvwall4":
				sss = "328.5cm x 70cm";
				te_bg.SetActive (true);
				break;
			case "fvwall6":
				sss = "327.5cm x 60cm";
				te_bg.SetActive (true);
				break;
			case "fvwall7":
				sss = "370cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fvwall8":
				sss = "363cm x 60cm";
				te_bg.SetActive (true);
				break;
			case "fvwall9":
				sss = "501.3cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fvwall10":
				sss = "";
				break;
			case "fvwall11":
				sss = "118cm x 50cm";
				te_bg.SetActive (true);
				break;
			case "fvwall12":
				sss = "328.5cm x 50cm";
				te_bg.SetActive (true);
				break;
			case "fvwall13":
				sss = "327.5cm x 60cm";
				te_bg.SetActive (true);
				break;
			case "fvwall14":
				sss = "363cm x 50cm";
				te_bg.SetActive (true);
				break;
			case "fvwall15":
				sss = "176cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "back":
				sss = "587cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "bath02":
				sss = "176cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "kitchen01":
				sss = "510.3cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "kitchen02":
				sss = "307cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "living01":
				sss = "418.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room1_01":
				sss = "427.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room1_02":
				sss = "427.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room3":
				sss = "246cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room4_01":
				sss = "363cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room2_02":
				sss = "109cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room4_02":
				sss = "363cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "bath01_01":
				sss = "176cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "bath02_01":
				sss = "246cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall1":
				sss = "126cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall2":
				sss = "377.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall3":
				sss = "";
				break;
			case "fhwall4":
				sss = "119cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall5":
				sss = "341cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall6":
				sss = "377.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall7":
				sss = "185cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall8":
				sss = "361.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall9":
				sss = "265cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall10":
				sss = "126cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall11":
				sss = "126cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall12":
				sss = "";
				break;
			case "fhwall13":
				sss = "209cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall14":
				sss = "94.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall15":
				sss = "83.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall16":
				sss = "77.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall17":
				sss = "77.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall18":
				sss = "118cm x 60cm";
				te_bg.SetActive (true);
				break;
			case "fhwall19":
				sss = "";
				break;
			case "fhwall20":
				sss = "377.5cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall21":
				sss = "";
				break;
			case "fhwall22":
				sss = "";
				break;
			case "fhwall23":
				sss = "126cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "fhwall24":
				sss = "71cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "bath01":
				sss = "206cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "living02":
				sss = "151cm x 265cm";
				te_bg.SetActive (true);
				break;
			case "room2_01":
				sss = "151cm x 265cm";
				te_bg.SetActive (true);
				break;
			//家具開始
			case "table coffee":
			case "table coffee(Clone)":
				sss = "180cm x 59cm x 40cm";
				te_bg.SetActive (true);
				break;
			case "coffee table":
			case "coffee table(Clone)":
				sss = "46.5cm x 46.5cm x 35cm";
				te_bg.SetActive (true);
				break;
			case "bookcase":
			case "bookcase(Clone)":
				sss = "120cm x 28cm x 237cm";
				te_bg.SetActive (true);
				break;
			case "box":
			case "box(Clone)":
				sss = "32cm x 34cm x 32cm";//
				te_bg.SetActive (true);
				break;
			case "Storage Box":
			case "Storage Box(Clone)":
				sss = "36cm x 27cm x 20cm";
				te_bg.SetActive (true);
				break;
			case "basket":
			case "basket(Clone)":
				sss = "36cm x 27cm x 23cm";
				te_bg.SetActive (true);
				break;
			case "trashcan":
			case "trashcan(Clone)":
				sss = "39cm x 55cm x 45cm";
				te_bg.SetActive (true);
				break;
			case "TV_cabinet":
			case "TV_cabinet(Clone)":
				sss = "90cm x 26cm x 45cm";
				te_bg.SetActive (true);
				break;
			case "floor lamp_f":
			case "floor lamp_f(Clone)":
				sss = "15.5cm x 15.5cm x 160cm";
				te_bg.SetActive (true);
				break;
			case "Single sofa_p":
			case "Single sofa_p(Clone)":
				sss = "99cm x 99cm x 83cm";
				te_bg.SetActive (true);
				break;
			case "bed_double_blue_b":
			case "bed_double_blue_b(Clone)":
				sss = "200cm x 150cm x 120cm";
				te_bg.SetActive (true);
				break;
			case "bed_double_red_b":
			case "bed_double_red_b(Clone)":
				sss = "206cm x 168cm x 100cm";
				te_bg.SetActive (true);
				break;
				//選單部分由此往下
			case "turn": 
				o_menu_turn = target.gameObject;
				o_menu_turn.SetActive (false);
				v3 = new Vector3 (target.transform.position.x, 2.5f, target.transform.position.z);
				sobj = Resources.Load ("menu");
				o_menu = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				o_menu.transform.Rotate (0, -90, 0);
				flag_menu = true;
				o_t1=o_menu.transform.GetChild(0).gameObject; //t1
				//o_t2=o_menu.transform.GetChild(1).gameObject; //t2
				o_menu_img=o_menu.transform.GetChild(2).gameObject; //img
				break;
			case "turn2":
				o_menu_turn = target.gameObject;
				o_menu_turn.SetActive (false);
				v3 = new Vector3 (target.transform.position.x, 2.5f, target.transform.position.z);
				sobj = Resources.Load ("menu");
				o_menu = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				o_menu.transform.Rotate (0, 0.707f, 0);
				flag_menu = true;
				o_t1=o_menu.transform.GetChild(0).gameObject; //t1
				//o_t2=o_menu.transform.GetChild(1).gameObject; //t2
				o_menu_img=o_menu.transform.GetChild(2).gameObject; //img
				break;
			case "turn3": 
				o_menu_turn = target.gameObject;
				o_menu_turn.SetActive (false);
				v3 = new Vector3 (target.transform.position.x, 2.5f, target.transform.position.z);
				sobj = Resources.Load ("menu");
				o_menu = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				o_menu.transform.Rotate (0, 90, 0);
				flag_menu = true;
				o_t1=o_menu.transform.GetChild(0).gameObject; //t1
				//o_t2=o_menu.transform.GetChild(1).gameObject; //t2
				o_menu_img=o_menu.transform.GetChild(2).gameObject; //img
				break;
			case "turn4":
				o_menu_turn = target.gameObject;
				o_menu_turn.SetActive (false);
				v3 = new Vector3 (target.transform.position.x, 2.5f, target.transform.position.z);
				sobj = Resources.Load ("menu");
				o_menu = Instantiate (sobj, v3, new Quaternion (0, 0, 0, 0)) as GameObject;
				o_menu.transform.Rotate (0, 0.707f+180, 0);
				flag_menu = true;
				o_t1=o_menu.transform.GetChild(0).gameObject; //t1
				//o_t2=o_menu.transform.GetChild(1).gameObject; //t2
				o_menu_img=o_menu.transform.GetChild(2).gameObject; //img
				break;
			case "next":
				if (ex == true) {
					if (mode == false) {
						if (i < 4) {
							i += 1;
							Debug.Log (i);
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_img [i]);
							o_t1.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_t2_1[i]);
							ex=false;
						} else {
							i = 4;
							Debug.Log (i);
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_img [i]);
							o_t1.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_t2_1[i]);
							ex=false;
						}
					} else {
						switch (i) {
						case 0:
							if (j < ui_f1.Length - 1) {
								j += 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f1[j]);
								ex=false;
							} else {
								j = ui_f1.Length - 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f1[j]);
								ex=false;
							}
							break;
						case 1:
							if (j < ui_f2.Length - 1) {
								j += 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f2[j]);
								ex=false;
							} else {
								j = ui_f2.Length - 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f2[j]);
								ex=false;
							}
							break;
						case 2:
							if (j < ui_f3.Length - 1) {
								j += 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f3[j]);
								ex=false;
							} else {
								j = ui_f3.Length - 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f3[j]);
								ex=false;
							}
							break;
						case 3:
							if (j < ui_f4.Length - 1) {
								j += 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f4[j]);
								ex=false;
							} else {
								j = ui_f4.Length - 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f4[j]);
								ex=false;
							}
							break;
						case 4:
							if (j < ui_f5.Length - 1) {
								j += 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f5[j]);
								ex=false;
							} else {
								j = ui_f5.Length - 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f5[j]);
								ex=false;
							}
							break;
						}
					}
				}

				break;
			case "ok":
				if (ex == true) {
					if (mode == false) {
						switch (i) {
						case 0:
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f1[0]);
							ex=false;
							break;
						case 1:
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f2[0]);
							ex=false;
							break;
						case 2:
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f3[0]);
							ex=false;
							break;
						case 3:
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f4[0]);
							ex=false;
							break;
						case 4:
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f5[0]);
							ex=false;
							break;
						}
						o_t1.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("pic/t2");
						mode = true;
					} else {
						Vector3 ok_v3;
						Object ok_sobj,ok_gobj;
						switch (i) {
						case 0:
							ok_v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
							ok_sobj = Resources.Load (f1[j]);
							ok_gobj = Instantiate (ok_sobj, ok_v3, new Quaternion (0, 0, 0, 0)) as GameObject;
							ex=false;
							break;
						case 1:
							ok_v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
							ok_sobj = Resources.Load (f2[j]);
							ok_gobj = Instantiate (ok_sobj, ok_v3, new Quaternion (0, 0, 0, 0)) as GameObject;
							ex=false;
							break;
						case 2:
							ok_v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
							ok_sobj = Resources.Load (f3[j]);
							ok_gobj = Instantiate (ok_sobj, ok_v3, new Quaternion (0, 0, 0, 0)) as GameObject;
							ex=false;
							break;
						case 3:
							ok_v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
							ok_sobj = Resources.Load (f4[j]);
							ok_gobj = Instantiate (ok_sobj, ok_v3, new Quaternion (0, 0, 0, 0)) as GameObject;
							ex=false;
							break;
						case 4:
							ok_v3 = new Vector3 (target.transform.position.x, 1.5f, target.transform.position.z);
							ok_sobj = Resources.Load (f5[j]);
							ok_gobj = Instantiate (ok_sobj, ok_v3, new Quaternion (0, 0, 0, 0)) as GameObject;
							ex=false;
							break;
						}
						flag_menu = false;
						Destroy (o_menu);
						o_menu_turn.SetActive (true);
						i = 0;
						j = 0;
						mode = false;
					}
				}
				break;
			case "pro":
				if (ex == true) {
					if (mode == false) {
						if (i > 0) {
							i -= 1;
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_img [i]);
							o_t1.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_t2_1[i]);
							ex=false;
						} else {
							i = 0;
							o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_img [i]);
							o_t1.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_t2_1[i]);
							ex=false;
						}
					} else {
						switch (i) {
						case 0:
							if (j >0) {
								j -= 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f1 [j]);
								ex=false;
							} else {
								j =0;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f1 [j]);
								ex=false;
							}
							break;
						case 1:
							if (j >0) {
								j -= 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f2 [j]);
								ex=false;
							} else {
								j =0;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f2 [j]);
								ex=false;
							}
							break;
						case 2:
							if (j >0) {
								j -= 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f3 [j]);
								ex=false;
							} else {
								j =0;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f3 [j]);
								ex=false;
							}
							break;
						case 3:
							if (j >0) {
								j -= 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f4 [j]);
								ex=false;
							} else {
								j =0;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f4 [j]);
								ex=false;
							}
							break;
						case 4:
							if (j >0) {
								j -= 1;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f5 [j]);
								ex=false;
							} else {
								j =0;
								o_menu_img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (ui_f5 [j]);
								ex=false;
							}
							break;
						}
					}
				}
				break;
			case "can":
				if (flag_menu == true) {
					flag_menu = false;
					Destroy (o_menu);
					o_menu_turn.SetActive (true);
					i = 0;
					j = 0;
					mode = false;
				}
				break;
			//切換screen
			case "choice":
				if (bhome == true) {
					switch (chome) {
					case 0:
						UnityEngine.SceneManagement.SceneManager.LoadScene (1);
						break;
					case 1:
						UnityEngine.SceneManagement.SceneManager.LoadScene (2);
						break;
					}
				}
				break;
			case "up":
				bhome = true;
				if (fhome == true) {
					chome++;
					fhome = false;
					if (chome >= home.Length) {
						chome = 0;
						skybox.material = Resources.Load (home [chome], typeof(Material)) as Material;
					} else {
						skybox.material = Resources.Load (home [chome], typeof(Material)) as Material;
					}
				}
				break;
			case "down":
				bhome = true;
				if (fhome == true) {
					chome--;
					fhome = false;
					if (chome < 0) {
						chome = home.Length - 1;
						skybox.material = Resources.Load (home [chome], typeof(Material)) as Material;
					} else {
						skybox.material = Resources.Load (home [chome], typeof(Material)) as Material;
					}
				}
				break;
			default:
				te_bg.SetActive (false);
				sss = "";
				break;
			}
			te.GetComponent<Text> ().text = sss;
			this.Invoke ("settext" , 5.0f); //幾秒後呼叫settext方法
		}

		public void settext(){
			te_bg.SetActive (false);
			te.GetComponent<Text> ().text = "";
		}
		/*public void settest(){ 1022
			test = true;
			move_text.SetActive (false);
			move_type = false;
		}*/

    }
}